﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Attributes;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Custom")]
    public class CustomController : Controller
    {
        [HttpGet]
        [ReqestLimit(Name = "Request limit", Seconds = 5)]
        public IActionResult GetStudent()
        {
            return Ok(new { Name = "John", SecondName = "Brown"});
        }
    }
}